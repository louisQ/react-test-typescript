module.exports = {

    parser: "@typescript-eslint/parser", // Specifies the ESLint parser
  
    parserOptions: {
  
      ecmaVersion: 2020, // Allows for the parsing of modern ECMAScript features
  
      sourceType: "module" // Allows for the use of imports
  
    },
  
    extends: [
  
      "plugin:react-hooks/recommended" // Uses the recommended rules from the @typescript-eslint/eslint-plugin
  
    ],

    plugins: [
        // ...
        "react-hooks"
      ],
    rules: {
  
        "react-hooks/rules-of-hooks": "error",
        "react-hooks/exhaustive-deps": "warn"
  
    }
  
  };