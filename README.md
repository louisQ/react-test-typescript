This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


## Steps setup to the App 
- npm install
- npm start 

## Required Packages 
some these will need to be installed and set up 
- [Material UI](https://material-ui.com/)
- [React Select](https://react-select.com/home)
- [Axios](https://github.com/axios/axios)
- Typescript

### Reqirements
- List Tasks
- Filter tasks by name, user, is completed
- Add Task
- Edit Task
- Delete Task

Im you have any questions please feel free to ask, also if you need any extra api's added please feel free to reach out 

API's Already Implemented
- GET api/todos
- GET api/todo/:id
- DELETE api/todo/:id/delete
- POST api/todo/create
- GET api/users
- GET api/user/id/todos
### Mockups 
![](./documentation/mockups.png)
