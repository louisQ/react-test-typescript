import React, { useEffect, useState } from 'react'
import InputName from '../FormUtils/inputName/InputName'
import TaskContainer from '../FormUtils/taskContainer/TaskContainer'
import UserCompleteBar from '../FormUtils/UserCompleteBar'
import SubmitButton from '../FormUtils/submitButton/SubmitButton'
import './ListTaskForm.scss'
import {connect} from 'react-redux'
import axios from 'axios'
import { OpenAddTask, UpdateTaskState } from '../../Redux/AddTaskState/AddTaskReducer'
import { TaskProps } from '../../Redux/Tasks/TaskActions'
import { RootState } from '../../Redux/rootReducer'
import { Dispatch } from 'redux'

interface ListTaskProps {
    update: Function,
    openAddTask: Function,
    allTasks: TaskProps[],
    children?: React.ReactNode
}

const ListTaskForm = ({update, openAddTask, allTasks}: ListTaskProps): JSX.Element => {
    const [tasks, setTask] = useState<TaskProps[]>([]) /*use to list data on the table*/
    const [searchTask, setSearchTask] = useState<TaskProps[]>([])
    const [inputValue, setInputValue] = useState('')
    const [completeStatus, setCompleteStatus] = useState(false)
    const [selectValue, setSelectValue] = useState('User')

    useEffect( () => {
        setTask(allTasks)
        setSearchTask(allTasks)
    },[allTasks, tasks])

    const findByName = (event : React.ChangeEvent<HTMLInputElement>) => {
        setCompleteStatus(false);
        const {name, value} = event.target;
        const searchValue = value;
        
        if (name === 'Find_Task') {
            setInputValue(searchValue)
            axios.get('api/todos')
            .then( ({data}) => {
                const findTask = data.todos.filter( (todo:TaskProps) => todo.name.toLowerCase().includes(value))
                setSearchTask(findTask)
            })
        }   
    }

    const beforeFindById = () => {
        setInputValue('');
        setCompleteStatus(false);
    }
    const findByUserId = async (user: {label: string, value: string}) => {
        beforeFindById();
        const url = 'api/user/' + user.value + '/todos'
        await axios.get<TaskProps[]>(url).then( ({data}) =>  console.log("server responses: ", data)
        ) 
        const foundTask = tasks.filter( (task:TaskProps) => task.user === user.value)
        setSearchTask(foundTask)
    }

    const findByCompleted = async (event: React.ChangeEvent<HTMLInputElement>) => {
        setInputValue('');
        setSelectValue("User")
        if (event.target.type === "checkbox") {
            const check = event.target.checked
            setCompleteStatus(check);

            await axios.get('api/todos').then( ({data}) => console.log("works"))
            setSearchTask( () => {
                const foundTasks = tasks.filter( (task:TaskProps) => task.isComplete === check)
                return foundTasks;
            })
        }
    }

    const handleUpdate = (task: TaskProps) => {
            update(task)
        }

    const handleDelete = (id: string) => {
        const deleteUrl = 'api/todo/' + id + '/delete'
        setSearchTask( pre => pre.filter( task => task.id !== id)) 
        axios.delete(deleteUrl)
        .then( response => console.log("deleted!")
        )
    }

    const OpenAddTaskState = () => {
        openAddTask()
    }

    return (
        <div className='list__task'>
            <div className='list__task-container'>
                <div className='list__task-container-header'>
                    <div className='inputname__container'>
                        <InputName value={inputValue} name="Find_Task" onChange={findByName}/>
                    </div>

                    <div className='usercomplete__container'>
                        <UserCompleteBar 
                            stateId={selectValue}
                            onSelect={findByUserId} 
                            check={completeStatus} 
                            isCompleted={findByCompleted}/>
                    </div>
                </div>

                <div className='list__task-container-list'>
                    <div className='list__task-container-list-bar'>
                        <span className='task__header-item'>Name</span>
                        <span className='task__header-item'>User</span>
                        <span className='task__header-item3'>
                            <span>Completed</span>
                        </span>
                        <span className='task__header-item4'>
                            <span className='action'>Actions</span>
                        </span>
                    </div>
            
                    {/* MAP THROUGH TASKS AND RETURN IN THE CONTAINER */}
                    { 
                        searchTask.length > 0 ? searchTask.map( (task) => 
                        <div key={task.id}>
                            <TaskContainer 
                                name={task.name}
                                userName={task.user}
                                isCompleted={task.isComplete}
                                onUpdate={() => handleUpdate(task)}
                                onDelete={() => handleDelete(task.id)}
                            />
                        </div>)
                        : null
                    }
                </div>

                <SubmitButton name='AddTask' onClick={OpenAddTaskState}/>
            </div>
        </div>
    )
}
const mapDispatchToProps = (dispatch: Dispatch) => ({
    update: (task: TaskProps) => dispatch(UpdateTaskState(task)),
    openAddTask: () => dispatch(OpenAddTask())
})
const mapStateToProps = (state: RootState) => ({
    allTasks: state.tasks.tasks
})
export default connect(mapStateToProps, mapDispatchToProps)(ListTaskForm);