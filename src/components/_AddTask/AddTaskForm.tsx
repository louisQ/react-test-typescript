import React, { useEffect, useState } from 'react'
import InputName from '../FormUtils/inputName/InputName';
import SubmitButton from '../FormUtils/submitButton/SubmitButton';
import UserCompleteBar from '../FormUtils/UserCompleteBar';
import './AddTaskForm.scss'
import {connect} from 'react-redux'
import Axios from 'axios';
import { CloseAddTask, addTaskProps } from '../../Redux/AddTaskState/AddTaskReducer';
import { UpdateTasks } from '../../Redux/Tasks/TaskActions';
import {TaskProps} from '../../Redux/Tasks/TaskActions'
import {Dispatch} from 'redux'
import {RootState} from '../../Redux/rootReducer'

interface ThisProps {
    addTaskState: addTaskProps,
    closeForm: Function,
    updateTasks: Function
}

const AddTaskForm = ({addTaskState, closeForm, updateTasks}:ThisProps): JSX.Element => {
    const [task, setTask] = useState<TaskProps>({id: '', name:'', user:'',isComplete: false})
    const [completeStatus, setCompleteStatus] = useState(false)
    const [inputValue, setInputValue] = useState('')
    const [defaultSelectValue, setSelectValue] = useState('User')
    const {display, currentTask, buttonName, tableName} = addTaskState

    const setTaskFC = () => {
        if (buttonName === "Update") {
            setTask(currentTask)
            setInputValue(currentTask.name);
            setSelectValue(`User Id: ${currentTask.user}`)
            setCompleteStatus(currentTask.isComplete)
        }
    }
    useEffect ( () => {
        setTaskFC();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[buttonName])
    
    /*ADD NEW TASK*/ 
    const addTask = async () => {
        if (task.user === '' || task.name === '') {
            alert('Please make sure you provided all information!')
            return;
        }
        else {
            const newTask = {
                id: null,
                name: task.name,
                userId: task.user,
                isComplete: task.isComplete            
            }
            await Axios.post('api/todo/create', newTask)
            .then( res =>        
                        res.statusText === "Created" ? 
                        alert("Added task successfully!") 
                        : alert("Could not save, try again!")
            );
            setInputValue('');
            setCompleteStatus(false)
            closeForm();
        }
    }
    /*UPDATE TASK */
    const updateTask = () => {
        const url = 'api/todo/' + task.id
        const taskUpdate = {
            id: parseInt(task.id),
            name: task.name,
            userId: task.user,
            completed: task.isComplete 
        }
        updateTasks(task)
        Axios.put(url,taskUpdate)
            .then( res => 
            res.status === 200 ? 
            alert("Updated successfully!") 
            : alert("Could not update, try again!")
         );
        setInputValue('')
        closeForm();
    }
    /*USE FOR CHANGE EVENT */
    const SetInputName = (value: string) => {
        setInputValue(value)
        setTask({...task, name: value})
    }
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = event.target;
        if (name === 'Add_Task') {
            SetInputName(value)
        }
    }

    const handleComplete = (event:React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.type === "checkbox") {
            const check = event.target.checked
            setTask({...task, isComplete:check})
            setCompleteStatus(check)
        }
    }

    const setSelectChange = (user: {label:string, value:string}) => {
        setSelectValue(user.label)
        setTask({...task, user: user.value})
    }

    const handleSelect = (user : {label:string, value:string}) => {
        setSelectChange(user)
    }

    const handleClick = () => {
        if (buttonName === "Save") {
           addTask()
        }
        else if (buttonName === "Update"){
           updateTask()
        }
    }

    return (
        <div className='add__task' style={{display: display}}>
            <div className='add__task-container'>
                <h3 className='title'>{tableName}</h3>

                <InputName value={inputValue} name="Add_Task" onChange={handleChange}/>

                <div className='user__complete-input'>
                    <UserCompleteBar 
                        stateId={defaultSelectValue} 
                        onSelect={handleSelect} 
                        check={completeStatus} 
                        isCompleted={handleComplete}
                    />
                </div>

                <SubmitButton name={buttonName} onClick={handleClick}/>
            </div>
        </div>
    )
}
const mapStateToProps = (state: RootState) => ({
    addTaskState: state.addTaskState.addTaskState,
})
const mapDispatchToProps = (dispatch:Dispatch) => ({
    closeForm: () => dispatch(CloseAddTask()),
    updateTasks: (task:TaskProps) => dispatch(UpdateTasks(task)),
})  
export default connect(mapStateToProps, mapDispatchToProps)(AddTaskForm);