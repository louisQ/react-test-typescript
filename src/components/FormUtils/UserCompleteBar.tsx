import React from 'react'
import CompleteButton from './completeButton/CompleteButton'
import './UserCompleteBar.scss'
import SelectUserButton from './selectUser/SelectUserButton'

interface ThisBarProps {
    stateId: string,
    onSelect: (event: any) => void,
    check: boolean,
    isCompleted: (event: React.ChangeEvent<HTMLInputElement>) => void,
}
const UserCompleteBar = ({ onSelect, check, isCompleted, stateId}: ThisBarProps) => {
    return (
        <div className='user__complete_bar'>
            <SelectUserButton stateId={stateId} onSelect={onSelect}/>
            <CompleteButton check={check} isCompleted={isCompleted}/>
        </div>
    )
}

export default UserCompleteBar;