import React from 'react'
import './InputName.scss'

interface InputNameProps {
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void,
    value: string,
    name: string
}
const InputName = ({onChange,value, name}: InputNameProps) => {
    return (
        <div className='input__name'>
            <label htmlFor='#text_input' className='input__label'>Project name</label>
            <input 
                className='input__content' 
                type='text' 
                id='text_input'
                name={name}
                onChange={onChange}
                value={value}
            />
        </div>
    )
}

export default InputName;