import React from 'react'
import './CompleteButton.scss'

interface CompleteButtonProps {
    check: boolean,
    isCompleted: (event: React.ChangeEvent<HTMLInputElement>) => void, 
}
const CompleteButton = ({check, isCompleted}:CompleteButtonProps): JSX.Element => {
    return (
        <div className='complete__button'>
            <label htmlFor='#complete_checkbox' className='label__switch'>Completed</label>

            <div className='switch__button' >
                <label className="switch">
                <input 
                    type="checkbox" 
                    name="completed"
                    checked={check}
                    onChange={isCompleted} 
                />
                <span className="slider"></span>
                <span className='slide-label'>Off</span>
                </label>
            </div>
        </div>
    )
}

export default CompleteButton;