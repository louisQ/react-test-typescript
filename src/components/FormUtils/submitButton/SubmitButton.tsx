import React from 'react'
import './SubmitButton.scss'

interface ButtonProps {
    name: string,
    onClick: (event: React.MouseEvent<HTMLButtonElement>) => void
}
const SubmitButton = ({name,onClick}: ButtonProps): JSX.Element => {

    return (
        <div className='submit__button'>
            <button 
                type="button" 
                className='submit__button-btn'
                onClick={onClick}
                >{name}
            </button>
        </div>
    )
}

export default SubmitButton
