import React from 'react'
import './TaskContainer.scss'

interface ThisProps {
    name: string,
    onUpdate: (e: React.MouseEvent<HTMLElement>) => void,
    onDelete: (e: React.MouseEvent<HTMLElement>) => void,
    userName: string,
    isCompleted: boolean,
}
const TaskContainer = ({name, onUpdate, onDelete, userName, isCompleted }: ThisProps): JSX.Element => {
    return (
        <div className='list__task-container-list-bar'>
            <span className='task__header-item'>{name}</span>
            <span className='task__header-item'>{userName}</span>
            <span className='task__header-item3'>
                {
                    isCompleted ? 
                    <span><i className="far fa-check-circle" id="check__icon"></i></span>
                    : <span className='not__completed'>not complete</span>
                }
            </span>
            <span className='task__header-item4'>
                <span>
                    <span><i    onClick={onDelete}
                                className="fas fa-times-circle" 
                                id="remove__icon"></i>
                    </span>
                    <span><i    onClick={onUpdate} 
                                className="fas fa-pencil-alt" 
                                id="update__icon"></i></span>
                </span> 
            </span>
        </div>
    )
}

export default TaskContainer;