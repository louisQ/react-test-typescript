import React from 'react'
import Select, {components, IndicatorProps} from 'react-select'
import {connect} from 'react-redux'
import './SelectUserButton.scss'
import { Users } from '../../../Redux/Users/UsersReducer';
import { RootState } from '../../../Redux/rootReducer';
import {ActionMeta} from 'react-select'

interface SelectButtonProps {
  onSelect: (value: any, actionMeta: ActionMeta<any>) => void,
  users: Users[],
  stateId: string
}
const SelectUserButton:React.FunctionComponent<SelectButtonProps> = ({onSelect, users, stateId}): JSX.Element => {
    
  const options = users.map( (user:Users) => ({label: user.firstName, value: user.id}))

    const DropdownIndicator = (
        props : IndicatorProps<any>
      ) => {
        return (
          <components.DropdownIndicator {...props} >
            <i className="fas fa-sort-down" id="arrow_icon"></i>
          </components.DropdownIndicator>
        );
      };
      const SingleValue = ({ children, ...props }:any) => (
        <components.SingleValue {...props}>
          {children}
        </components.SingleValue>
      );
    return (
        <div className='select__user'>
            <label htmlFor='react-select-container'>Users</label>
            <Select 
                SingleValue={SingleValue}
                name="user"
                components={{ DropdownIndicator }}
                options={options}
                onChange={onSelect}
                maxMenuHeight={120}
                placeholder={stateId}
                isSearchable={false}
            />
        </div>
    )
}
const mapStateToProps = (state: RootState) => ({
    users: state.users.users
})

export default connect(mapStateToProps)(SelectUserButton);