import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {makeServer} from './Server/server'
import {Provider} from 'react-redux'
import {createStore, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import rootReducer from './Redux/rootReducer'

const server = makeServer()
console.log(server) 

const middleWare = [];
if (process.env.NODE_ENV === "development") {middleWare.push(thunk, logger)}
const store = createStore(rootReducer, applyMiddleware(...middleWare));

ReactDOM.render(
   <Provider store={store}>
      <App/>
    </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
