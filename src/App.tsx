import React, { useEffect} from 'react';
import './App.scss';
import AddTaskForm from './components/_AddTask/AddTaskForm';
import ListTaskForm from './components/_ListTask/ListTaskForm';
import axios from 'axios'
import {GetUsers, Users} from './Redux/Users/UsersReducer'
import {connect} from 'react-redux'
import { FetchTasks } from './Redux/Tasks/TaskActions';
import {Dispatch} from 'redux'
import {TaskProps} from './Redux/Tasks/TaskActions'

interface AppProps {
  fetchUsers: Function,
  fetchTasks: Function
}
const App = ({fetchUsers, fetchTasks}:AppProps): JSX.Element => {
  
  const GetTasks = async () => {
    await axios.get('api/todos')
    .then( ({data}) => fetchTasks(data.todos))
  }
  const GetUsers = async () => {
    await axios.get('api/users')
    .then(({data}) => fetchUsers(data.users));
  }

  useEffect( () => {
    const GetData = async () => {
      await GetTasks()
      await GetUsers()
    }
    GetData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])

  return (
          <div className="App">
            <div className='App__content'>
              <ListTaskForm/>
              <AddTaskForm/>
            </div>
          </div>
  );
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
  fetchUsers: (users:Users[]) => dispatch(GetUsers(users)),
  fetchTasks: (tasks:TaskProps[]) => dispatch(FetchTasks(tasks))
})
export default connect(null, mapDispatchToProps)(App);
