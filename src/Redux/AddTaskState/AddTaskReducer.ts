import { TaskProps } from "../Tasks/TaskActions";
import {ActionTypes} from '../Tasks/ActionTypes'

export interface addTaskProps {
    tableName: string,
    display: string,
    currentTask: TaskProps,
    buttonName: string
}
export interface AddTaskStateProps {
    addTaskState: addTaskProps
}
const initialState:AddTaskStateProps = {
    addTaskState: {
        tableName: "",
        display: 'none',
        currentTask: {id: '', user: '', name:'', isComplete:false},
        buttonName: ''
    }
};

export interface OpenAddTaskType {
    type: 
    ActionTypes.openAddTask,
}
export interface CloseAddTaskType {
    type: ActionTypes.closeAddTask
}
export interface UpdateTaskState {
    type: ActionTypes.updateTaskState
    payload: TaskProps
}

type Action = OpenAddTaskType | CloseAddTaskType | UpdateTaskState;

export const OpenAddTask = () => ({
    type: ActionTypes.openAddTask
})
export const CloseAddTask = () => ({
    type: ActionTypes.closeAddTask
})
export const UpdateTaskState = (task:TaskProps) => ({
    type: ActionTypes.updateTaskState ,
    payload: task
})

const AddTaskReducer = (state = initialState, action: Action) => {
    switch (action.type) {
        case ActionTypes.updateTaskState:
            return {
                ...state, 
                addTaskState: {
                    ...state,
                    tableName: 'Update Task',
                    display: 'flex',
                    currentTask: action.payload,
                    buttonName: "Update"
                }
            }
        case ActionTypes.openAddTask:
            return {
                ...state,
                addTaskState: {
                    ...state,
                    display: 'flex',
                    buttonName: "Save",
                    tableName: 'Add Task',
                    currentTask: {id: '', user:'', name:'', isComplete:false}
                }
            } 
        case ActionTypes.closeAddTask:
            return {...state, 
                addTaskState: {
                    ...state,
                    display: 'none',
                    buttonName:'',
                    tableName:'',
                    currentTask: {id: '', user:'', name:'', isComplete:false}
                }
            }

        default:
            return initialState
    }
}

export default AddTaskReducer;