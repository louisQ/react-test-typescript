import {combineReducers} from 'redux'
import userReducer, { userState} from './Users/UsersReducer';
import taskReducer from './Tasks/TaskReducer';
import AddTaskReducer, {AddTaskStateProps} from './AddTaskState/AddTaskReducer';
import { TaskState } from './Tasks/TaskActions';

interface rootReducerState {
    addTaskState: AddTaskStateProps,
    users: userState,
    tasks: TaskState
}
const rootReducer = combineReducers<rootReducerState>({
    addTaskState: AddTaskReducer,
    users : userReducer,
    tasks: taskReducer
});
export default rootReducer; 
export type RootState = ReturnType<typeof rootReducer>