import { TaskState, TaskProps} from './TaskActions'
import {Action, ActionTypes} from './ActionTypes'

 const initialTasks: TaskState = {
    tasks: [],
};

const taskReducer = (state = initialTasks, action: Action) => {
    switch (action.type) {
        case ActionTypes.fetchType:
            return {
                ...state,
                tasks: action.payload
            }
        case ActionTypes.updateTasks:
            const taskNeedUpdate = state.tasks.findIndex( (task: TaskProps) => 
                                                    task.id === action.payload.id)
            const newTasks:TaskProps[] = Object.assign([], state.tasks)
            newTasks[taskNeedUpdate] = action.payload

            return {
                ...state,
                tasks: newTasks
            }
        default:
            return state;
    }
}

export default taskReducer;