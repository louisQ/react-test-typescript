import {FetchAction, UpdateAction } from './TaskActions'

export enum ActionTypes {
    fetchType,
    updateTasks,
    openAddTask,
    updateTaskState,
    closeAddTask
}

export type Action = FetchAction | UpdateAction
