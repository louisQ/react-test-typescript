import { ActionTypes } from "./ActionTypes"

export interface TaskProps {
    id: string,
    name: string,
    user: string,
    isComplete: boolean
}
export interface NewTaskProps {
    name: string,
    user: string,
    isComplete: boolean
}

export interface TaskState {
    tasks: TaskProps[],
}
export interface FetchAction {
    type: ActionTypes.fetchType,
    payload: TaskProps[]
}

export interface UpdateAction {
    type: ActionTypes.updateTasks,
    payload: TaskProps
}
export const FetchTasks = (tasks:TaskProps[]):FetchAction => ({
    type: ActionTypes.fetchType,
    payload: tasks
})  

export const UpdateTasks = (task: TaskProps): UpdateAction => ({
    type: ActionTypes.updateTasks,
    payload: task
})

