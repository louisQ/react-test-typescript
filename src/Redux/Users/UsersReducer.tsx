
export interface Users {
    id: number,
    firstName: string,
    lastName: string
}

export interface userState {
    users: Users[]
}
const initialUsers:userState = {
    users: []
};

export interface GetUsersAction {
    type: string,
    payload: Users[]
}
export const GetUsers = (users:Users[]):GetUsersAction => ({
    type: "GET_USERS",
    payload: users
});


const userReducer = (state = initialUsers, action:GetUsersAction) => {
    switch (action.type) {
        case "GET_USERS":
            return {
                ...state,
                users: action.payload
            }

        default:
            return state;
    }
}

export default userReducer;